const std = @import("std");
const expect = std.testing.expect;

pub fn dotprod(x: []const i32, y: []const i32) i32 {
    var s: i32 = 0;
    for (x, y) |x_i, y_i| {
        s += x_i * y_i;
    }
    return s;
}

pub fn main() void {
    std.debug.print("Hello, {s}!\n", .{"World"});

    const v1 = [_]i32{ 1, 2, 3, 4, 5 };
    const v2 = [_]i32{ 1, -1, 2, -2, 3 };

    const dot = dotprod(v1[0..5], v2[0..5]);

    std.debug.print("Vector 1: {any}\n", .{v1});
    std.debug.print("Vector 2: {any}\n", .{v2});
    std.debug.print("Dot product {}\n", .{dot});

    const rand = std.crypto.random;
    const random_number = rand.float(f32);
    //const random_number_formatted = std.format(w, "{d:.3}", .{random_number});
    std.debug.print("Random number: {d:.5}\n", .{random_number});
}
