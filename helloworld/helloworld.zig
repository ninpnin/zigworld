const std = @import("std");
const expect = std.testing.expect;

const number_five: i32 = 5; // signed 32-bit constant
var big_number: u32 = 3000; // unsigned 32-bit variable
const word = [4]u8{ 'p', 'a', 'j', 'a' };
const Direction = enum { pohjoinen, ita, etela, lansi };
var ilmansuunta: Direction = Direction.ita; // unsigned 32-bit variable

pub fn main() void {
    std.debug.print("Hello, {s}!\n", .{"World"});
    std.debug.print("Hello, {}!\n", .{number_five});
    std.debug.print("Hello, {}!\n", .{big_number});
    std.debug.print("Hello, {s}!\n", .{word});

    if (big_number >= 4000) {
        std.debug.print("Big number!\n", .{});
    } else {
        std.debug.print("Small number lmao\n", .{});
    }

    for (word) |character| {
        const subword = [1]u8{character};
        std.debug.print("{s} \n", .{subword});
        //_ = character;
    }
    std.debug.print("Hello, {}!\n", .{ilmansuunta});
}

test "always succeeds" {
    try expect(true);
}

test "out of bounds" {
    const a = [3]u8{ 1, 2, 3 };
    var index: u8 = 5;
    const b = a[index];
    _ = b;
}
